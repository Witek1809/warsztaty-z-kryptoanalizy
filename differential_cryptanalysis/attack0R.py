#%% Definicja funkcji pomocnicznych
# -*- coding: utf-8 -*-


import math
import time


from tytus import Tytus
from romek import Romek
from atomek import Atomek
from utils import rand_bytes, print_as_hexes, convert_to_hex, get_zeros


def attack0R_toy_spn(cipher, pairs, differential_path):
    """Atak typu 0R na szyfr Tytus, Romek lub Atomek.

    Args:
        cipher (<class 'tytus.Tytus'>, <class 'atomek.Atomek'> lub <class 'romek.Romek'>): 
          instancja klasy Tytus, Romek lub Atomek.
        pairs (list): lista par wybranych do ataku.
        differential_path (list): ścieżka różnicowa w postaci listy list 
          np. [[0x2A, 0x67, 0x81, 0x7C], [0x00, 0x00, 0x20, 0x60], [0x30, 0x00, 0x30, 0x30], [0x60, 0x20, 0x00, 0x00]
    """

    assert str(type(cipher)) == "<class 'tytus.Tytus'>" or str(type(cipher)) == "<class 'atomek.Atomek'>" or str(type(cipher)) == "<class 'romek.Romek'>"

    histogram_last_subkey = [
        [0] * 16, # hex pozycja 0
        [0] * 16, # hex pozycja 1
        [0] * 16, # hex pozycja 2
        [0] * 16, # hex pozycja 3
        [0] * 16, # hex pozycja 4
        [0] * 16, # hex pozycja 5
        [0] * 16, # hex pozycja 6
        [0] * 16, # hex pozycja 7
    ]

    char_1 = cipher.mix(differential_path[len(differential_path)-2])
    char_2 = differential_path[len(differential_path)-1]
    ch_hex = convert_to_hex(char_1)

    for p in pairs:

        p0 = p[0]
        p1 = p[1]
        c0 = cipher.encrypt(p0)
        c1 = cipher.encrypt(p1)

        c0_hex = convert_to_hex(c0)
        c1_hex = convert_to_hex(c1)
        
        for i in range(0, len(histogram_last_subkey), 1):
            if c0_hex[i] ^ c1_hex[i] != 0x0:
                for key in range(0x00, 0x10, 0x01):
                    if (cipher.__sbox_inv__[c0_hex[i] ^ key] ^ cipher.__sbox_inv__[c1_hex[i] ^ key]) == ch_hex[i]:
                        histogram_last_subkey[i][key] += 1
    
    print()
    print("CHAR_R-1", end=" ") 
    print_as_hexes(char_1, end="\n")
    
    print("CHAR_R-0", end=" ")
    print_as_hexes(char_2, end="\n")
    
    print("SUBKEY  ", end=" ")
    print_as_hexes(cipher.__subkeys__[cipher.__rounds__], end="\n\n")

    print("subkey value [", end="")
    for v in range(0, 16, 1): print("   %s" % hex(v)[2:].upper(), end = " ")
    print("]")
    
    for i in range(0, len(histogram_last_subkey), 1):
        print("Histogram %2d [" % i, end="")
        for v in histogram_last_subkey[i]: print("%4d" % v, end = " ")
        print("]")
    print()


#%% Program główny  
if __name__ == "__main__":

    # Charakterystyka różnicowa Romek 4 rund
    """
    Wejście   | 0x00030000
    --------- | ----------
    Runda   1 | 0x01000101
    Runda   2 | 0x00030000
    Runda   3 | 0x01000101
    Runda   4 | 0x00030000
    """

    # rounds = 4
    # differential_path = [
    #     [0x00, 0x03, 0x00, 0x00],
    #     [0x01, 0x00, 0x01, 0x01],
    #     [0x00, 0x03, 0x00, 0x00],
    #     [0x01, 0x00, 0x01, 0x01],
    #     [0x00, 0x03, 0x00, 0x00],
    # ]
    # cipher = Romek(rounds)

    # Charakterystyka różnicowa Atomek 6 rund
    """
    Wejście   | 0x21000008
    --------- | ----------
    Runda   1 | 0x04000000
    Runda   2 | 0x00004000
    Runda   3 | 0x00000005
    Runda   4 | 0x000C0000
    Runda   5 | 0x00000400
    Runda   6 | 0x00000500
    """

    # rounds = 6
    # differential_path = [
    #     [0x21, 0x00, 0x00, 0x08],
    #     [0x04, 0x00, 0x00, 0x00],
    #     [0x00, 0x00, 0x40, 0x00],
    #     [0x00, 0x00, 0x00, 0x05],
    #     [0x00, 0x0C, 0x00, 0x00],
    #     [0x00, 0x00, 0x04, 0x00],
    #     [0x00, 0x00, 0x05, 0x00],
    # ]
    # cipher = Atomek(rounds)

    # Charakterystyka różnicowa Tytus 3 rundy
    """
    Wejście   | 0x2A67817C
    --------- | ----------
    Runda   1 | 0x00002060
    Runda   2 | 0x30003030
    Runda   3 | 0x60200000
    """

    rounds   = 3
    differential_path = [
        [0x2A, 0x67, 0x81, 0x7C],
        [0x00, 0x00, 0x20, 0x60],
        [0x30, 0x00, 0x30, 0x30],
        [0x60, 0x20, 0x00, 0x00]
    ]
    cipher   = Tytus(rounds)
    
    charIN   = differential_path[0]
    charOUT  = differential_path[len(differential_path)-1]
        
    pairs    = []
    iterator = 0
    last_it  = 0
    pairs_n  = 10
    mkey     = rand_bytes(int(512/8))

    print()
    print("Differantial path:")
    for i in range(0, len(differential_path), 1):
        print("%5d | " % i, end="")
        print_as_hexes(differential_path[i], end="\n")
    print()
    
    cipher.set_key(mkey)

    start = time.time()
    globT = time.time()
    var_s = 0.00

    print("Rozpoczęto proces poszukiwania %d par. Liczba rund %d" % (pairs_n, cipher.__rounds__), type(cipher))

    while len(pairs) < pairs_n:

        p0 = rand_bytes(4)
        p1 = [ p0[i] ^ charIN[i] for i in range(0, len(charIN), 1)]
        c0 = cipher.encrypt(p0)
        c1 = cipher.encrypt(p1)
        dd = [ c0[i] ^ c1[i] for i in range(0, len(charIN), 1) ]

        if dd == charOUT:
            
            pairs.append([p0, p1])

            end     = time.time()
            elapsed = end - start
            start   = time.time()
            var_s  += math.log2(iterator - last_it)

            print(
                "Znaleziono parę %5d. Przeszukano przestrzeń o rozmiarze  2^{%5.2f} %10.2f [s]" % 
                (len(pairs), math.log2(iterator - last_it), elapsed)
            )

            last_it = iterator
        
        iterator += 1
    
    var_s  /= pairs_n
    end     = time.time()
    elapsed = end - globT

    print("Zakończono poszukiwanie par. Liczba sprawdzonych par 2^{%5.2f} %10.2f [s]" % (math.log2(iterator), elapsed))
    print("Poprawną parę znajdywano średnio po analizie         2^{%5.2f} par" % (var_s))

    if str(type(cipher)) == "<class 'tytus.Tytus'>" or str(type(cipher)) == "<class 'atomek.Atomek'>" or str(type(cipher)) == "<class 'romek.Romek'>":
        _ = attack0R_toy_spn(cipher, pairs, differential_path)
