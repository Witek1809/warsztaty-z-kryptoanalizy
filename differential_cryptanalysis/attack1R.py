#%% Definicja funkcji pomocnicznych
# -*- coding: utf-8 -*-


import math
import time
import secrets


from tytus import Tytus
from utils import rand_bytes, print_as_hexes, convert_to_hex, get_zeros


tytus_m1R = [
    [0x20, 0x30, 0x50, 0x60, 0xA0, 0xC0, 0xE0],
    [0x50, 0xA0, 0xB0, 0xD0, 0xE0, 0xF0],
    [0x10, 0x30, 0x40, 0x60, 0x90, 0xB0, 0xD0, 0xF0],
    [0x10, 0x50, 0x60, 0x70, 0x80, 0x90, 0xB0, 0xF0]
] # INFO(WD) tablica jest odpowiednia dla charakterystyki 0x60200000


def char_checker_v1(char_1, char_2):

    if len(char_1) == len(char_2):

        for i in range(0, len(char_1), 1):

            if ((char_1[i] == 0x00) and (char_2[i] == 0x00)):
                continue

            elif (char_1[i] != 0x00 and char_2[i] != 0x00):

                if ((((char_1[i] >> 4) & 0x0F) != 0x00) and (((char_2[i] >> 4) & 0x0F) != 0x00)) or ((((char_1[i] >> 4) & 0x0F) == 0x00) and (((char_2[i] >> 4) & 0x0F) == 0x00)):
                    if ((((char_1[i]) & 0x0F) != 0x00) and (((char_2[i]) & 0x0F) != 0x00)) or ((((char_1[i]) & 0x0F) == 0x00) and (((char_2[i]) & 0x0F) == 0x00)):
                        continue
                    else:
                        return False
                else:
                    return False
            else:
                return False

        return True
    else:
        return False


def char_checker_v2(char_1, m=tytus_m1R):

    for i in range(0, len(char_1), 1):
        if char_1[i] in tytus_m1R[i]:
            continue
        else:
            return False

    return True


def attack1R_tytus(cipher, pairs, differential_path):
    """Atak typu 1R na szyfr Tytus.

    Args:
        cipher (<class 'tytus.Tytus'>): instancja klasy Tytus.
        pairs (list): lista par wybranych do ataku.
        differential_path (list): ścieżka różnicowa w postaci listy list np.      
    """

    assert str(type(cipher)) == "<class 'tytus.Tytus'>"

    histogram_last_subkey = [
        [0] * 16, # hex pozycja 0
        [0] * 16, # hex pozycja 1
        [0] * 16, # hex pozycja 2
        [0] * 16, # hex pozycja 3
        [0] * 16, # hex pozycja 4
        [0] * 16, # hex pozycja 5
        [0] * 16, # hex pozycja 6
        [0] * 16, # hex pozycja 7
    ]

    char_1 = cipher.mix(differential_path[len(differential_path)-1])
    ch_hex = convert_to_hex(char_1)

    for p in pairs:

        p0 = p[0]
        p1 = p[1]
        c0 = cipher.encrypt(p0)
        c1 = cipher.encrypt(p1)

        c0_hex = convert_to_hex(c0)
        c1_hex = convert_to_hex(c1)
        
        for i in range(0, len(histogram_last_subkey), 1):
            if c0_hex[i] ^ c1_hex[i] != 0x0:
                for key in range(0x00, 0x10, 0x01):
                    if (cipher.__sbox_inv__[c0_hex[i] ^ key] ^ cipher.__sbox_inv__[c1_hex[i] ^ key]) == ch_hex[i]:
                        histogram_last_subkey[i][key] += 1
    
    print()
    print("CHAR_R-1", end=" ") 
    print_as_hexes(char_1, end="\n")
    
    # print("CHAR_R-0", end=" ")
    # print_as_hexes(char_2, end="\n")
    
    print("SUBKEY  ", end=" ")
    print_as_hexes(cipher.__subkeys__[cipher.__rounds__], end="\n\n")

    print("subkey value [", end="")
    for v in range(0, 16, 1): print("   %s" % hex(v)[2:].upper(), end = " ")
    print("]")
    
    for i in range(0, len(histogram_last_subkey), 1):
        print("Histogram %2d [" % i, end="")
        for v in histogram_last_subkey[i]: print("%4d" % v, end = " ")
        print("]")
    print()


#%% Program główny  
if __name__ == "__main__":

    # Charakterystyka różnicowa Tytus 3 rundy  
    """ 
    Wejście   | 0x2A67817C
    --------- | ----------
    Runda   1 | 0x00002060
    Runda   2 | 0x30003030
    Runda   3 | 0x60200000
    """

    differential_path = [
        [0x2A, 0x67, 0x81, 0x7C],
        [0x00, 0x00, 0x20, 0x60],
        [0x30, 0x00, 0x30, 0x30],
        [0x60, 0x20, 0x00, 0x00]
    ]
    
    rounds   = 4
    charIN   = [0x2A, 0x67, 0x81, 0x7C]
    charOUT  = [0x60, 0x20, 0x00, 0x00]
    
    pairs    = []
    iterator = 0
    last_it  = 0
    pairs_n  = 500
    cipher   = Tytus(rounds)
    mkey     = rand_bytes(int(512/8))
    char_tmp = cipher.mix(charOUT)

    print()
    print("Differantial path:")
    for i in range(0, len(differential_path), 1):
        print("%5d | " % i, end="")
        print_as_hexes(differential_path[i], end="\n")
    print()
    
    cipher.set_key(mkey)

    start = time.time()
    globT = time.time()
    var_s = 0.00

    print("Rozpoczęto proces poszukiwania %d par ... " % pairs_n)

    while len(pairs) < pairs_n:

        p0 = rand_bytes(4)
        p1 = [ p0[i] ^ charIN[i] for i in range(0, len(charIN), 1)]
        c0 = cipher.encrypt(p0)
        c1 = cipher.encrypt(p1)
        dd = [ c0[i] ^ c1[i] for i in range(0, len(charIN), 1) ]

        if char_checker_v2(dd): # char_checker(dd, char_tmp):
            
            pairs.append([p0, p1])

            end     = time.time()
            elapsed = end - start
            start   = time.time()
            var_s  += math.log2(iterator - last_it)

            print(
                "Znaleziono parę %5d. Przeszukano przestrzeń o rozmiarze  2^{%5.2f} %10.2f [s]" % 
                (len(pairs), math.log2(iterator - last_it), elapsed)
            )

            last_it = iterator
            
        
        iterator += 1

    var_s  /= pairs_n
    end     = time.time()
    elapsed = end - globT

    print("Zakończono poszukiwanie par. Liczba sprawdzonych par 2^{%5.2f} %10.2f [s]" % (math.log2(iterator), elapsed))
    print("Poprawną parę znajdywano średnio po analizie         2^{%5.2f} par" % (var_s))

    if str(type(cipher)) == "<class 'tytus.Tytus'>":
        _ = attack1R_tytus(cipher, pairs, differential_path)
