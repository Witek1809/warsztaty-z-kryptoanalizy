#%% Definicja szyfru Atomek
# -*- coding: utf-8 -*-


class Atomek:
    """Klasa Atomek implementuje szyfr oparty o sieć SPN o nazwie Atomek. 
    """

    def __init__(self, rounds):
        """Konstruktor klasy Atomek.

        Args:
            rounds (int): liczba rund z przedziału <2, 15>. 
        """
        assert rounds > 1 and rounds < 16    
        self.__block_size__ = 32
        self.__key_size__   = 512
        self.__sbox__       = [0x7, 0x4, 0xA, 0x9, 0x1, 0xF, 0xB, 0x0, 0xC, 0x3, 0x2, 0x6, 0x8, 0xE, 0xD, 0x5]
        self.__sbox_inv__   = [0x7, 0x4, 0xA, 0x9, 0x1, 0xF, 0xB, 0x0, 0xC, 0x3, 0x2, 0x6, 0x8, 0xE, 0xD, 0x5]
        self.__subkeys__    = [[0x00, 0x00, 0x00, 0x00] for i in range(0, rounds + 1, 1)]
        self.__perm__       = [0x09, 0x0a, 0x06, 0x08, 0x19, 0x13, 0x0b, 0x04, 0x05, 0x0e, 0x1c, 0x15, 0x14, 0x17, 0x01, 0x1f, 0x11, 0x1e, 0x02, 0x10, 0x18, 0x16, 0x1a, 0x0f, 0x03, 0x00, 0x1d, 0x12, 0x07, 0x0d, 0x1b, 0x0c]
        self.__rounds__     = rounds
        self.__perm_inv__   = [0x00] * len(self.__perm__)

        for i in range(0, len(self.__perm__), 1): self.__perm_inv__[self.__perm__[i]] = i
    
    @staticmethod        
    def __permutation__(vbytes, perm_tab):

        j          = 0
        bin_stream = ""
        bin_state  = ""
        state      = [0x00] * 4

        for v in vbytes: bin_stream += bin(v)[2:].zfill(8)
        for p in perm_tab: bin_state += bin_stream[p]

        for i in range(0, 4, 1):
            idx = 8*i
            state[j] = int(bin_state[idx:idx+8], 2)
            j += 1

        return state

    def sub(self, vbytes):
        """Przekształcenie kryptograficzne odpowiedzialne za konfuzję. 

        Args:
            vbytes (list): stan wejściowy, lista 4 bajtów. 

        Returns:
            list: stan wyjściowy, lista 4 bajtów.
        """
        return [(self.__sbox__[(b & 0xF0) >> 4] << 4) ^ self.__sbox__[b & 0x0F] for b in vbytes]
    
    def sub_inv(self, vbytes):
        """Przekształcenie kryptograficzne odwrotne do przekształcenia odpowiedzialnego za konfuzję. 

        Args:
            vbytes (list): stan wejściowy, lista 4 bajtów. 

        Returns:
            list: stan wyjściowy, lista 4 bajtów.
        """
        return [(self.__sbox_inv__[(b & 0xF0) >> 4] << 4) ^ self.__sbox_inv__[b & 0x0F] for b in vbytes]
    
    def mix(self, vbytes):
        """Przekształcenie kryptograficzne odpowiedzialne za dyfuzję. 

        Args:
            vbytes (list): stan wejściowy, lista 4 bajtów. 

        Returns:
            list: stan wyjściowy, lista 4 bajtów.
        """
        return Atomek.__permutation__(vbytes, self.__perm_inv__)
    
    def mix_inv(self, vbytes):
        """Przekształcenie kryptograficzne odwrotne do przekształcenia odpowiedzialnego za dyfuzję. 

        Args:
            vbytes (list): stan wejściowy, lista 4 bajtów. 

        Returns:
            list: stan wyjściowy, lista 4 bajtów.
        """
        return Atomek.__permutation__(vbytes, self.__perm__)
        
    def set_key(self, mkey):
        """Metoda odpowiedzialna za ustawienie kluczy rundowych.

        Args:
            mkey (list): klucz główny, lista 64 bajtów.   

        Returns:
            bool: True jeżeli inicjalizacja kluczy rundowych poprawna, False w p.p.. 
        """
        assert len(mkey)*8 == self.__key_size__
        for i in range(0, self.__rounds__ + 1, 1):
            for j in range(0, 4, 1):
                self.__subkeys__[i][j] = mkey[4*i + j]
        return True

    def encrypt_round(self, plaintext, rkey):
        """Metoda odpowiedzialna za wykonanie funkcji rundy podczas szyfrowania.

        Args:
            plaintext (list): stan wejściowy, lista 4 bajtów.
            rkey (list): klucz rundowy, lista 4 bajtów.

        Returns:
            list: stan wyjściowy, lista 4 bajtów.
        """
        state = self.mix(plaintext)
        state = self.sub(state)
        return [state[j] ^ rkey[j] for j in range(0, 4, 1)]

    def decrypt_round(self, ciphertext, rkey):
        """Metoda odpowiedzialna za wykonanie funkcji rundy podczas deszyfrowania.

        Args:
            plaintext (list): stan wejściowy, lista 4 bajtów.
            rkey (list): klucz rundowy, lista 4 bajtów.

        Returns:
            list: stan wyjściowy, lista 4 bajtów.
        """
        state = self.sub_inv(ciphertext)
        state = self.mix_inv(state)
        return [state[j] ^ rkey[j] for j in range(0, 4, 1)]

    def encrypt(self, plaintext):
        """Metoda odpowiedzialna za szyfrowanie.

        Args:
            plaintext (list): tekst jawny, lista 4 bajtów.

        Returns:
            list: szyfrogram, lista 4 bajtów.
        """
        state = [plaintext[i] ^ self.__subkeys__[0][i] for i in range(0, 4, 1)]
        for i in range(1, self.__rounds__ + 1, 1):
            state = self.encrypt_round(state, self.__subkeys__[i])
        return state

    def decrypt(self, ciphertext):
        """Metoda odpowiedzialna za deszyfrowanie.

        Args:
            ciphertext (list): szyfrogram, lista 4 bajtów.

        Returns:
            list: tekst jawny, lista 4 bajtów.
        """
        state = [ciphertext[i] ^ self.__subkeys__[self.__rounds__][i] for i in range(0, 4, 1)]
        for i in range(self.__rounds__ - 1, -1, -1):
            state = self.decrypt_round(state, self.__subkeys__[i])
        return state


#%% Testy jednostkowe
if __name__ == "__main__":


    import unittest
    

    from utils import rand_bytes


    class TestingAtomek(unittest.TestCase):

        def test_encryption(self, n=1000):
            cipher = Atomek(15)
            for i in range(0, n, 1):
                k = rand_bytes(64)
                self.assertTrue(cipher.set_key(k))
                p = rand_bytes(4)
                c = cipher.encrypt(p)
                m = cipher.decrypt(c)
                self.assertTrue(m==p)
    
    unittest.main()
