#%% Definicja funkcji pomocnicznych
# -*- coding: utf-8 -*-


import random
import secrets


def rand_bytes(n):
    """Funkcja losuje n bajtów. 

    Args:
        n (int): liczba bajtów.  

    Returns:
        list: lista n bajtów.
    """
    t = secrets.token_bytes(n)
    return [ t[i] for i in range(0, n, 1)] # return [ random.randint(0, 255) for i in range(0, n, 1) ]


def print_as_hexes(vbytes, end="\n"):
    """Funkcja wypisuje listę bajtów w postaci ciągu znaków heksadecymalnych poprzedzonych znakiem '0x'.

    Args:
        vbytes (list): lista bajtów. 
        end (str, optional): znak końca linii. Domyślny to "\n".
    """
    h = ""
    for v in vbytes: 
        h += hex(v)[2:].zfill(2)
    h = "0x" + h.upper()
    print(h, end=end)


def convert_to_hex(vbytes):
    """Funkcja konwertuje listę bajtów do listy zawierającej wartości z przedziału <0, 15>.      

    Args:
        vbytes (list): lista bajtów.

    Returns:
        list: lista wartości z przedziału <0, 15>. 
    """
    h = []
    for b in vbytes:
        h.append((b >> 4) & 0x0F)
        h.append(b & 0x0F)
    return h


def get_zeros(n):
    """Funkcja zwraca listę bajtów wypełnioną zerami.   

    Args:
        n (int): liczba bajtów.

    Returns:
        list: lista n bajtów. 
    """
    return [ 0x00 for i in range(0, n, 1)]
