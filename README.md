# Funkcjonalność repozytorium.  

W skryptach języka python zaimplementowano pokazowe ataki wykorzystujące współczesne techniki kryptoanalizy. Do celów pokazowych wykorzystano 3 szyfry oparte o sieć SPN: *Atomek*, *Romek*, *Tytus*.      

# Wymagania.

Python w wersji >= 3.7.1